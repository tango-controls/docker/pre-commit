FROM python:3.11

LABEL MAINTAINER "TANGO Controls Team <contact@tango-controls.org>"

RUN useradd -l -ms /bin/bash tango

# Avoid: fatal: detected dubious ownership in repository
RUN git config --system --add safe.directory '*'

ENV PRE_COMMIT_VERSION 3.7.0

RUN python3 -m venv /venv \
  && /venv/bin/python3 -m pip install --no-cache-dir -U pip setuptools wheel           \
  && /venv/bin/python3 -m pip install --no-cache-dir pre-commit==${PRE_COMMIT_VERSION}

ENV PATH /venv/bin:$PATH

USER tango
