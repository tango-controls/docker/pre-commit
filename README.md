# pre-commit docker image

Python [docker] image to run [pre-commit] in CI.

Docker pull command:

```bash
docker pull registry.gitlab.com/tango-controls/docker/pre-commit
```

[docker]: https://www.docker.com
[pre-commit]: https://pre-commit.com
